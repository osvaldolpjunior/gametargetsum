
<!-- PROJECT LOGO -->

  <h3 align="center">Target Sum Game App</h3>

  <p align="center">
    A simple target sum game with countdown.
    <br />
  </p>
</p>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#contact">Contact</a></li>    
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

<p align="center">
  <img src="/images/GameTargetSum.gif" alt="Bake Sale App Screen" width="250" height="550"/>
</p>


During the React Native Essential Training course, I built this application. Some concepts learned:
- Creating style sheets
- Working with flexbox 
- Adding data points to a list
- Animation
- Adding touch responses
- And more.

[Course Link](https://www.linkedin.com/learning/react-native-essential-training/working-with-react-native?u=2201753)

### Built With

This was you built using.

- [React Native](https://reactnative.dev/)
- [Metro](https://facebook.github.io/metro/)
- [Babel](https://babeljs.io/)

<!-- GETTING STARTED -->

## Getting Started

To get a local copy up and running follow these simple example steps.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.

- npm
  ```sh
  npm install npm@latest -g
  ```

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/osvaldolpjunior/gametargetsum.git
   ```
2. Install NPM packages
   ```sh
   npm install
   ```
3. Execute using React Native / Metro
   ```
   react-native start
   ```

<!-- CONTACT -->

## Contact

Osvaldo Junior - osvaldo.junior@and.digital

Project Link: [https://gitlab.com/osvaldolpjunior/gametargetsum](https://gitlab.com/osvaldolpjunior/gametargetsum)

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
