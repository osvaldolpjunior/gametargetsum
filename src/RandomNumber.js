import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, TouchableOpacity} from 'react-native';

class RandomNumber extends React.Component {
  static propTypes = {
    id: PropTypes.number.isRequired,
    number: PropTypes.number.isRequired,
    isDisabled: PropTypes.bool.isRequired,
    onPress: PropTypes.func.isRequired,
  };

  handlePress = () => {
    if (this.props.isDisabled) {
      return;
    }
    this.props.onPress(this.props.id);
  };

  render() {
    return (
      <TouchableOpacity onPress={this.handlePress}>
        <Text style={[styles.random, this.props.isDisabled && styles.disabled]}>
          {' '}
          {this.props.number}
        </Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  random: {
    backgroundColor: '#4798f7',
    width: 100,
    fontSize: 35,
    textAlign: 'center',
    marginHorizontal: 25,
    marginVertical: 25,
    borderWidth: 2,
  },
  disabled: {
    opacity: 0.3,
  },
});

export default RandomNumber;
